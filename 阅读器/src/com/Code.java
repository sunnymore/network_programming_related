package com;

import java.io.*;

public class Code {

    public static String getUtf8(InputStream inputStream) throws IOException {

        InputStreamReader inputStreamReader = new InputStreamReader(inputStream,"utf-8");

        StringBuilder sb = new StringBuilder();

        int c;

        while ((c = inputStreamReader.read()) != -1){

            sb.append((char)c);

        }

        return sb.toString();

    }

    public static String getUtf8_2(InputStream inputStream) throws IOException {

        Reader r = new InputStreamReader(inputStream,"utf-8");

        r = new BufferedReader(r,1024);

        StringBuilder sb = new StringBuilder();

        int c;

        while ((c = r.read()) != -1){
            sb.append((char)c);
        }

        return sb.toString();

    }

    public static void main(String[] args) {

        try (FileInputStream fileInputStream = new FileInputStream("aaa")){

            System.out.println(getUtf8_2(fileInputStream));

        }catch (Exception ex){
            System.out.println(ex.getMessage());
        }

    }

}
