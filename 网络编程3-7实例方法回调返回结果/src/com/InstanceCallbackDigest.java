package com;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.security.DigestInputStream;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public class InstanceCallbackDigest implements Runnable {

    private String fileName;

    private InstanceCallbackDigestUserInterface instanceCallbackDigestUserInterface;

    public InstanceCallbackDigest(String fileName, InstanceCallbackDigestUserInterface instanceCallbackDigestUserInterface) {
        this.fileName = fileName;
        this.instanceCallbackDigestUserInterface = instanceCallbackDigestUserInterface;
    }

    @Override
    public void run(){

        try {
            FileInputStream in = new FileInputStream(fileName);

            MessageDigest sha = MessageDigest.getInstance("SHA-256");

            DigestInputStream digestInputStream = new DigestInputStream(in,sha);

            while (digestInputStream.read() != -1) ;

            byte[] bytes = sha.digest();

            digestInputStream.close();

            instanceCallbackDigestUserInterface.receive(bytes);

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

}
