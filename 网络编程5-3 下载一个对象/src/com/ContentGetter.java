package com;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;

public class ContentGetter {

    public static void main(String[] args) {

        for(String arg : args){

            try {
                URL url = new URL(arg);

                Object o = url.getContent();

                System.out.println(o.getClass().getName());


            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }


        }

    }

}
