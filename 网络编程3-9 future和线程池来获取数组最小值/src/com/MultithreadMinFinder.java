package com;

import java.util.concurrent.*;

public class MultithreadMinFinder {

    public static void main(String[] args) {

        int[] data = {2,3,2,3,24,3,4,2,4,234,3,4,3,1,32,3,5,523};

        FindMinTask task1 = new FindMinTask(data,0,(data.length-1)/2);

        FindMinTask task2 = new FindMinTask(data,(data.length+1)/2, data.length-1);

        ExecutorService threadPool = Executors.newFixedThreadPool(2);

        Future<Integer> future1 = threadPool.submit(task1);

        Future<Integer> future2 = threadPool.submit(task2);

        try {
            System.out.println(Math.min(future1.get(),future2.get()));
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }

    }

}
