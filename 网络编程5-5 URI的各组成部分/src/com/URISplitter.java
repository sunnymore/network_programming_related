package com;

import java.net.URI;
import java.net.URISyntaxException;

public class URISplitter {

    public static void main(String[] args) {

        for(String arg : args){

            try {
                URI uri = new URI(arg);

                System.out.println("the uri is " + uri);

                System.out.println("the schema is " + uri.getScheme());

                if(uri.isOpaque()){
                    //非层次
                    System.out.println("isOpache");

                    System.out.println("the schema special part is " + uri.getSchemeSpecificPart());

                    System.out.println("the fragment is " + uri.getFragment());

                }else{
                    //层次，先强制解析
                    uri = uri.parseServerAuthority();

                    System.out.println("notOpache");

                    System.out.println("the host is " + uri.getHost());

                    System.out.println("the userinfo is " + uri.getUserInfo());

                    System.out.println("the port is " + uri.getPort());

                    System.out.println("the path is " + uri.getPath());

                    System.out.println("the query is " + uri.getQuery());

                    System.out.println("the fragment is " + uri.getFragment());

                }

            } catch (URISyntaxException e) {
                e.printStackTrace();
            }

        }

    }

}
