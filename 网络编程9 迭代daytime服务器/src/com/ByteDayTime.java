package com;

import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketAddress;
import java.util.Date;

public class ByteDayTime {

    public final static int PORT = 1113;

    public static void main(String[] args) {

        long differenceBetweenEpchs = 2208988800L;

        try (ServerSocket serverSocket = new ServerSocket(PORT)){

            while (true){

                try (Socket socket = serverSocket.accept()){

                    OutputStream writer = socket.getOutputStream();

                    Date date = new Date();

                    long msSince1970 = date.getTime();

                    long secondsSince1970 = msSince1970/1000;

                    long secondsSince1900 = secondsSince1970 + differenceBetweenEpchs;

                    byte[] time = new byte[4];

                    time[0] = (byte) ((secondsSince1900 & 0x00000000FF000000L) >> 24);

                    time[1] = (byte) ((secondsSince1900 & 0x0000000000FF0000L) >> 16);

                    time[2] = (byte) ((secondsSince1900 & 0x000000000000FF00L) >> 8);

                    time[3] = (byte) (secondsSince1900 & 0x00000000000000FFL);

                    writer.write(time);

                    writer.flush();

                }

            }

        } catch (IOException e) {
            e.printStackTrace();
        }

    }

}
