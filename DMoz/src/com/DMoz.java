package com;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;

public class DMoz {

    public static void main(String[] args) {

        StringBuilder target = new StringBuilder("http://vip.iqiyi.com/firstsix-new-pc.html?");

        for(int i=0;i<args.length;i+=2){

            target.append(args[i]);

            target.append(URLEncoder.encode(args[i+1]));

        }

        try {
            URL url = new URL(target.toString());

            try(InputStream in = new BufferedInputStream(url.openStream())) {

                InputStreamReader reader = new InputStreamReader(in,"UTF-8");

                int c;

                while ((c=reader.read()) != -1){
                    System.out.print((char)c);
                }

            } catch (IOException e) {
                e.printStackTrace();
            } ;

        } catch (MalformedURLException e) {
            e.printStackTrace();
        }

    }

}
