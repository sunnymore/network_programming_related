package com;

import javax.xml.bind.DatatypeConverter;

public class CallbackDigestUserInterface {

    public static void receiveDigest(String fileName,byte[] bytes){

        StringBuilder sb = new StringBuilder(fileName);

        sb.append(": ");

        sb.append(DatatypeConverter.printHexBinary(bytes));

        System.out.println(sb);

    }

    public static void main(String[] args){

        for(String arg : args){

            CallbackDigest callbackDigest = new CallbackDigest(arg);

            callbackDigest.start();

        }

    }

}
