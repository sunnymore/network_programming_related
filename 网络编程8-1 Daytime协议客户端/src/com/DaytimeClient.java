package com;

import java.io.*;
import java.net.Socket;
import java.net.UnknownHostException;

public class DaytimeClient {

    public static void main(String[] args) {

        String hostname = args.length>0?args[0]:"time.nist.gov";

        try (Socket socket = new Socket(hostname,13)){

            socket.setSoTimeout(15000);

            InputStream in = socket.getInputStream();

            Reader reader = new InputStreamReader(in,"ASCII");

            int c;

            while ((c=reader.read()) != -1)
                System.out.print((char)c);

        } catch (UnknownHostException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

}
