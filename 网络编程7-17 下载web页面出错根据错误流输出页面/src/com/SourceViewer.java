package com;

import java.io.*;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;

public class SourceViewer {


    public static void printFromStream(InputStream in){

        try(BufferedInputStream bin = new BufferedInputStream(in)){

            Reader reader = new InputStreamReader(bin);

            int c;

            while ((c=reader.read()) != -1)
                System.out.print((char)c);

        } catch (IOException e) {
            e.printStackTrace();
        }

    }


    public static void main(String[] args) {

        if(args.length > 0){

            try {
                URL url = new URL(args[0]);

                HttpURLConnection huc = (HttpURLConnection) url.openConnection();

                if(huc.usingProxy()){

                    System.out.println("使用了代理服务器");

                }else{

                    System.out.println("未使用代理服务器");

                }

                try(InputStream in = huc.getInputStream()){

                    printFromStream(in);

                }catch (IOException ex){

                    printFromStream(huc.getErrorStream());

                }

            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }

        }

    }

}
