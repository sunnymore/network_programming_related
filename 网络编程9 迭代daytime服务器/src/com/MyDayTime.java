package com;

import java.io.BufferedWriter;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.Date;

public class MyDayTime {

    public static void main(String[] args) {

        try (ServerSocket serverSocket = new ServerSocket(1113)){

            while (true){

                try (Socket socket = serverSocket.accept()){

                    Writer writer = new BufferedWriter(new OutputStreamWriter(socket.getOutputStream(),"UTF-8"));

                    writer.write(new Date().toString()+"\r\n");

                    writer.flush();

                }

            }

        } catch (IOException e) {
            e.printStackTrace();
        }



    }

}
