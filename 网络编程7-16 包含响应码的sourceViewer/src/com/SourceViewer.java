package com;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

public class SourceViewer {

    public static void main(String[] args) {

        if(args.length > 0){

            try {
                URL url = new URL(args[0]);

                HttpURLConnection uc = (HttpURLConnection) url.openConnection();

                int code = uc.getResponseCode();

                String message = uc.getResponseMessage();

                System.out.println("HTTP/1.x " + code + " " + message);

                for(int i=1;;i++){

                    String name = uc.getHeaderFieldKey(i);

                    String value = uc.getHeaderField(i);

                    if(name == null || value == null)
                        break;

                    System.out.println(name+":"+value);

                }

                System.out.println();

                try(InputStream in = uc.getInputStream()){

                    Reader reader = new InputStreamReader(in,"UTF-8");

                    int c;

                    while ((c=reader.read()) != -1)
                        System.out.print((char)c);



                }

            } catch (MalformedURLException e) {


            } catch (IOException e) {
                e.printStackTrace();
            }

        }

    }

}
