package com;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.security.DigestInputStream;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public class CallbackDigest extends Thread {

    private String fileName;

    public CallbackDigest(String fileName) {
        this.fileName = fileName;
    }

    @Override
    public void run(){

        try {
            FileInputStream in = new FileInputStream(fileName);

            MessageDigest sha = MessageDigest.getInstance("SHA-256");

            DigestInputStream digestInputStream = new DigestInputStream(in,sha);

            while (digestInputStream.read() != -1) ;

            digestInputStream.close();

            byte[] res = sha.digest();

            CallbackDigestUserInterface.receiveDigest(fileName,res);

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }
}
