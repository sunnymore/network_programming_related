package com;

import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.Date;
import java.util.concurrent.ThreadPoolExecutor;

public class MultiThreadDayTime {

    private final static int PORT = 1113;

    public static void main(String[] args) {

        try(ServerSocket serverSocket = new ServerSocket(PORT)) {

            while (true){

                Socket conn = serverSocket.accept();

                Thread t = new DayTimeThread(conn);

                t.start();

            }

        } catch (IOException e) {
            e.printStackTrace();
        } ;

    }

    public static class DayTimeThread extends Thread{

        private Socket conn;

        public DayTimeThread(Socket conn) {
            this.conn = conn;
        }

        @Override
        public void run() {

            try {
                Writer writer = new OutputStreamWriter(conn.getOutputStream());

                writer.write(new Date().toString() + "\r\n");

                writer.flush();

            } catch (IOException e) {
                e.printStackTrace();
            }finally {

                try {
                    conn.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }

            }

        }
    }

}
