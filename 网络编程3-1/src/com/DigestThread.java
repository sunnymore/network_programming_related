package com;

import sun.nio.ch.sctp.SctpNet;

import javax.xml.bind.DatatypeConverter;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.security.DigestInputStream;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Scanner;

public class DigestThread extends Thread {

    private String fileName;

    public DigestThread(String fileName) {
        this.fileName = fileName;
    }

    @Override
    public void run(){

        try {
            //System.out.println(fileName);

            FileInputStream in = new FileInputStream(fileName);

            MessageDigest sha = MessageDigest.getInstance("SHA-256");

            DigestInputStream din = new DigestInputStream(in,sha);

            while (din.read() != -1) ;

            din.close();

            byte[] digest = sha.digest();

            StringBuilder sb = new StringBuilder(fileName);

            sb.append(": ");

            sb.append(DatatypeConverter.printHexBinary(digest));

            System.out.println(sb);

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }


    public static void main(String[] args) {

//        try (FileInputStream fileInputStream = new FileInputStream("aaa");){
//
//        } catch (FileNotFoundException e) {
//            e.printStackTrace();
//        } catch (IOException e) {
//            e.printStackTrace();
//        }

        Scanner in = new Scanner(System.in);

        while(in.hasNext()){

            DigestThread digestThread = new DigestThread(in.next());

            digestThread.run();

        }

    }

}
