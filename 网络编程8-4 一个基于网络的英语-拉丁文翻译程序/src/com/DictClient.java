package com;

import java.io.*;
import java.net.Socket;
import java.net.UnknownHostException;

public class DictClient {

    private static final String server = "dict.org";

    private static final int port = 2628;

    private static final int timeout = 15000;

    public static void main(String[] args) {

        try (Socket socket = new Socket(server,port)){

            socket.setSoTimeout(timeout);

            OutputStream out = socket.getOutputStream();

            Writer writer = new OutputStreamWriter(out,"UTF-8");

            writer = new BufferedWriter(writer);

            InputStream in = socket.getInputStream();

            BufferedReader reader = new BufferedReader(new InputStreamReader(in,"UTF-8"));

            for(String word : args){

                define(word,reader,writer);

            }

            writer.write("quit\r\n");

            writer.flush();

        } catch (UnknownHostException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    private static void define(String word, BufferedReader reader, Writer writer) throws IOException {

        writer.write("DEFINE eng-lat " + word + "\r\n");

        writer.flush();

        String line;

        while ((line = reader.readLine()) != null){

            if(line.startsWith("250")){
                return;
            }else if(line.startsWith("552")){
                System.out.println("No defination found for " + word);
                return;
            }
            else if(line.matches("\\d\\d\\d .*"))continue;
            else if(line.trim().equals("."))continue;
            else System.out.println(line);

        }

    }

}
