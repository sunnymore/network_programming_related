package com;

import java.net.MalformedURLException;
import java.net.URL;

public class URLSplitter {

    public static void main(String[] args) {

        for(String arg: args){

            try {
                URL url = new URL(arg);

                System.out.println("the url is " + url);

                System.out.println("the protocal is " + url.getProtocol());

                System.out.println("the user is " + url.getUserInfo());

                String host = url.getHost();

                if(host != null){

                    int atSign = host.indexOf('@');

                    if(atSign != -1)
                        host = host.substring(atSign + 1);

                    System.out.println("the host is " + host);

                }

                System.out.println("the path is " + url.getPath());

                System.out.println("the port is " + url.getPort());

                System.out.println("the default port is " + url.getDefaultPort());

                System.out.println("the ref is " + url.getRef());

                System.out.println("the query is " + url.getQuery());

            } catch (MalformedURLException e) {
                e.printStackTrace();
            }


        }

    }

}
