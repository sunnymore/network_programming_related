package com;

import java.net.Inet4Address;
import java.net.InetAddress;
import java.net.UnknownHostException;

public class Inet {

    public static void main(String[] args) {

        try {

            //通过域名解析,创建InetAddress对象
            InetAddress inetAddress = InetAddress.getByName("www.baidu.com");

            System.out.println(inetAddress);

            //通过ip解析,创建InetAddress对象
            InetAddress inetAddress5 = InetAddress.getByName("125.216.242.50");

            System.out.println(inetAddress5);

            byte[] address = new byte[]{(byte) 125,(byte)216,(byte)242,(byte)50};

            //创建InetAddress对象
            InetAddress inetAddress1 = InetAddress.getByAddress(address);

            System.out.println(inetAddress1);

            InetAddress inetAddress2 = InetAddress.getByAddress("afang",address);

            System.out.println(inetAddress2);

            InetAddress[] inetAddresses = InetAddress.getAllByName("www.baidu.com");

            for(InetAddress inet: inetAddresses){

                System.out.println(inet);

            }

            InetAddress me = InetAddress.getLocalHost();

            System.out.println(me);

        } catch (UnknownHostException e) {
            e.printStackTrace();
        }

    }

}
