package com;

import java.util.concurrent.Callable;

public class FindMinTask implements Callable<Integer> {

    private int[] data;

    private int start;

    private int end;

    public FindMinTask(int[] data, int start, int end) {
        this.data = data;
        this.start = start;
        this.end = end;
    }

    @Override
    public Integer call() throws Exception {

        int res = Integer.MAX_VALUE;

        for(int i=start;i<=end;i++){

            if(data[i] < res){

                res = data[i];

            }

        }

        return res;

    }



}
