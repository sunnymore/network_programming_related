package com;

import java.io.IOException;
import java.io.InputStream;
import java.net.Socket;
import java.net.UnknownHostException;

public class TestByteDayTime {

    public static void main(String[] args) {

        try(Socket socket = new Socket("localhost",1113)){

            InputStream in = socket.getInputStream();

            int c;

            byte[] b = new byte[4];

            while ((c = in.read(b)) != -1){
                for(byte bb : b){
                    System.out.println(bb+256);
                }
            }

        } catch (UnknownHostException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

}
