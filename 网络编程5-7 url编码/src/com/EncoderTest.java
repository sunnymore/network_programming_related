package com;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

public class EncoderTest {

    public static void main(String[] args) {

        try {
            System.out.println(URLEncoder.encode("This string has spaces","UTF-8"));
            System.out.println(URLEncoder.encode("This*string*has*spaces","UTF-8"));
            System.out.println(URLEncoder.encode("This%string%has%spaces","UTF-8"));
            System.out.println(URLEncoder.encode("This+string+has+spaces","UTF-8"));
            System.out.println(URLEncoder.encode("This/string/has/spaces","UTF-8"));
            System.out.println(URLEncoder.encode("This\"string\"has\"spaces","UTF-8"));
            System.out.println(URLEncoder.encode("This:string:has:spaces","UTF-8"));
            System.out.println(URLEncoder.encode("This~string~has~spaces","UTF-8"));
            System.out.println(URLEncoder.encode("This(string)has(spaces)","UTF-8"));
            System.out.println(URLEncoder.encode("This.string.has.spaces","UTF-8"));
            System.out.println(URLEncoder.encode("This=string=has=spaces","UTF-8"));
            System.out.println(URLEncoder.encode("This&string&has&spaces","UTF-8"));
            System.out.println(URLEncoder.encode("This|string|has|spaces","UTF-8"));


        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }

    }

}
