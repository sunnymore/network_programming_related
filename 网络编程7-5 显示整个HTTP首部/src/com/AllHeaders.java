package com;

import java.io.*;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;

public class AllHeaders {

    public static void main(String[] args) {

        if(args.length > 0){

            try {
                URL url = new URL(args[0]);

                URLConnection urlConnection = url.openConnection();

                for(int i=0;;i++){

                    String header = urlConnection.getHeaderField(i);

                    if(header == null)
                        break;

                    System.out.println(urlConnection.getHeaderFieldKey(i) + ":" + header);

                }

            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }

        }

    }

}
