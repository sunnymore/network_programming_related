package com;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.net.SocketAddress;

public class SocketInfo {

    public static void main(String[] args) {

        for(String arg: args){

            try {
                Socket socket = new Socket(arg,80);

                System.out.println("connect to " + socket.getInetAddress() + ":" + socket.getPort() + " from " + socket.getLocalAddress() + ":" + socket.getLocalPort());

            } catch (IOException e) {
                e.printStackTrace();
            }

        }

    }

}
