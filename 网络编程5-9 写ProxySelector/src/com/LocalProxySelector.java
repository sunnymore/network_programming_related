package com;

import java.io.IOException;
import java.net.*;
import java.util.ArrayList;
import java.util.List;

public class LocalProxySelector extends ProxySelector {

    private List<URI> failed = new ArrayList<>();

    @Override
    public List<Proxy> select(URI uri) {

        List<Proxy> result = new ArrayList<>();

        if(failed.contains(uri) || !"http".equalsIgnoreCase(uri.getScheme())){
            //在失败列表里，或者不是http协议
            result.add(Proxy.NO_PROXY);
        }else{
            //是http协议，且未连接失败过
            SocketAddress socketAddress = new InetSocketAddress("proxy.example.com",8000);
            Proxy proxy = new Proxy(Proxy.Type.HTTP,socketAddress);
            result.add(proxy);
        }

        return result;
    }

    @Override
    public void connectFailed(URI uri, SocketAddress sa, IOException ioe) {
        failed.add(uri);
    }
}
