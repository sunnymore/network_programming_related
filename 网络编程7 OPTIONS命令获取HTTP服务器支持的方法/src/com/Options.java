package com;

import sun.net.www.protocol.http.HttpURLConnection;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;

public class Options {

    public static void main(String[] args) {

        if(args.length > 0){

            try {
                URL url = new URL(args[0]);

                HttpURLConnection uc = (HttpURLConnection) url.openConnection();

                uc.setRequestMethod("OPTIONS");

                uc.setRequestProperty("Connection","close");

                uc.setRequestProperty("Accept","text/html,image/gif,image/jpeg,*;q=.2,*/*;q=.2");

                for(int i=0;;i++){

                    String value = uc.getHeaderField(i);

                    if(value == null){

                        break;

                    }

                    String key = uc.getHeaderFieldKey(i);

                    System.out.println(key + ":" + value);

                }

            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }

        }

    }

}
