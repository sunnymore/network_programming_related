package com;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.net.SocketAddress;

public class SocketConn {

    public static void main(String[] args) {

        String host = args.length>0?args[0]:"www.timegoin.com";

        Socket socket = new Socket();

        SocketAddress socketAddress = new InetSocketAddress(host,80);

        try {
            socket.connect(socketAddress);
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

}
