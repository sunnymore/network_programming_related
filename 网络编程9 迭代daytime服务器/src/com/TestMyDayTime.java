package com;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.net.Socket;

public class TestMyDayTime {

    public static void main(String[] args) {

        try (Socket socket = new Socket("localhost",1113)){

            InputStream in = socket.getInputStream();

            Reader reader = new InputStreamReader(in);

            int c;

            while ((c=reader.read()) != -1)
                System.out.print((char)c);

        } catch (IOException e) {
            e.printStackTrace();
        }

    }

}
