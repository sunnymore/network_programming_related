package com;

import com.sun.org.apache.bcel.internal.generic.Select;

import java.io.IOException;
import java.net.*;
import java.nio.ByteBuffer;
import java.nio.channels.SelectionKey;
import java.nio.channels.Selector;
import java.nio.channels.ServerSocketChannel;
import java.nio.channels.SocketChannel;
import java.util.Iterator;
import java.util.Set;

public class IntgenServer {

    private final static int DEFAULT_PORT = 1919;

    private int port;

    public IntgenServer(int port) {
        this.port = port;
    }

    public void start(){

        try (ServerSocketChannel channel = ServerSocketChannel.open()){

            ServerSocket serverSocket = channel.socket();

            SocketAddress addr = new InetSocketAddress(port);

            serverSocket.bind(addr);

            channel.configureBlocking(false);

            Selector selector = Selector.open();

            channel.register(selector, SelectionKey.OP_ACCEPT);

            while (true){

                selector.select();

                Set<SelectionKey> s = selector.selectedKeys();

                Iterator<SelectionKey> iter = s.iterator();

                while (iter.hasNext()){

                    SelectionKey key = iter.next();

                    iter.remove();

                    if(key.isAcceptable()){

                        ServerSocketChannel serverSocketChannel = (ServerSocketChannel) key.channel();

                        SocketChannel client = serverSocketChannel.accept();

                        System.out.println(client + " has connected");

                        client.configureBlocking(false);

                        ByteBuffer buffer = ByteBuffer.allocate(4);

                        buffer.putInt(0);

                        //buffer.put((byte) '\r');
                        //buffer.put((byte) '\n');

                        buffer.flip();

                        SelectionKey key2 = client.register(selector,SelectionKey.OP_WRITE);

                        key2.attach(buffer);

                    }

                    if(key.isWritable()){

                        SocketChannel client = (SocketChannel) key.channel();

                        ByteBuffer buffer = (ByteBuffer) key.attachment();

                        if(!buffer.hasRemaining()){

                            buffer.rewind();

                            int num = buffer.getInt();

                            System.out.println(num);

                            buffer.clear();

                            buffer.putInt(num+1);

                            //buffer.put((byte) '\r');
                            //buffer.put((byte) '\n');

                            buffer.flip();

                        }

                        client.write(buffer);

                    }

                }

            }

        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    public static void main(String[] args) {

        int port;

        if(args.length == 0) port = DEFAULT_PORT;
        else port = Integer.parseInt(args[0]);

        IntgenServer server = new IntgenServer(port);

        server.start();

    }

}
