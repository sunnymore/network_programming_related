package com;

import java.io.*;
import java.net.*;

public class Whois {

    public final static int DEFAULT_PORT = 43;
    public final static String DEFAULT_HOST = "whois.internic.net";

    private InetAddress host;

    private int port = DEFAULT_PORT;

    public Whois(InetAddress host, int port) {
        this.host = host;
        this.port = port;
    }

    public Whois(InetAddress host) {
        this.host = host;
    }

    public Whois(String host,int port) throws UnknownHostException {
        this(InetAddress.getByName(host),port);
    }

    public Whois(String host) throws UnknownHostException {
        this(InetAddress.getByName(host),DEFAULT_PORT);
    }

    public Whois() throws UnknownHostException {
        this(DEFAULT_HOST,DEFAULT_PORT);
    }

    public enum SearchFor{
        ANY("Any"),NETWORK("Network"),PERSON("Person"),HOST("Host"),DOMAIN("Domain"),ORGANIZATION("Organization"),GROUP("Group"),GATEWAY("Gateway"),ASN("Asn");
        private String label;

        SearchFor(String label) {
            this.label = label;
        }
    }

    public enum SearchIn{
        ALL(""),NAME("Name"),MAILBOX("Mailbox"),HANDLE("!");
        private String label;

        SearchIn(String label) {
            this.label = label;
        }
    }

    public String lookUpNames(String target,SearchFor category,SearchIn group,boolean exactMatch) throws IOException {
        String suffix = "";
        if(exactMatch) suffix = ".";

        String prefix = category.label + " " + group.label;
        String query = prefix + target + suffix;

        Socket socket = new Socket();
        SocketAddress addr = new InetSocketAddress(host,port);

        try {
            socket.connect(addr);

            OutputStream out = socket.getOutputStream();

            Writer writer = new OutputStreamWriter(out,"ASCII");

            InputStream in = socket.getInputStream();

            BufferedReader reader = new BufferedReader(new InputStreamReader(in));

            writer.write(query + "\r\n");

            writer.flush();

            StringBuilder res = new StringBuilder();

            String line;

            while ((line = reader.readLine()) != null) {
                res.append(line);
                res.append("\r\n");
            }

            return res.toString();

        }finally {
            socket.close();
        }
    }

    public void setHost(String host) throws UnknownHostException {
        this.host = InetAddress.getByName(host);
    }

    public InetAddress getHost() {
        return host;
    }

    public static void main(String[] args) {
        //test
        String query = "Test";

        SearchIn searchIn = SearchIn.ALL;

        SearchFor searchFor = SearchFor.DOMAIN;

        try {
            Whois whois = new Whois();

            String res = whois.lookUpNames(query,searchFor,searchIn,true);

            System.out.println(res);

        } catch (UnknownHostException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }


}
