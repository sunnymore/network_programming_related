package com;

import javax.xml.bind.DatatypeConverter;

public class InstanceCallbackDigestUserInterface {

    private String fileName;

    private byte[] digest;

    public InstanceCallbackDigestUserInterface(String fileName) {
        this.fileName = fileName;
    }

    public void calculate(){

        InstanceCallbackDigest instanceCallbackDigest = new InstanceCallbackDigest(fileName,this);

        Thread t = new Thread(instanceCallbackDigest);

        t.start();

    }

    public void receive(byte[] digest){

        this.digest = digest;

        System.out.println(this);

    }

    @Override
    public String toString() {

        StringBuilder sb = new StringBuilder(fileName);

        sb.append(": ");

        if(digest != null){

            sb.append(DatatypeConverter.printHexBinary(digest));

        }else{

            sb.append("digest is not ready");

        }

        return sb.toString();
    }

    public static void main(String[] args) {

        for(String arg: args){

            InstanceCallbackDigestUserInterface i = new InstanceCallbackDigestUserInterface(arg);

            i.calculate();

        }

    }
}
