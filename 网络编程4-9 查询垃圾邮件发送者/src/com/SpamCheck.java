package com;

import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.UnknownHostException;

public class SpamCheck {

    private static String query = "sbl.spamhaus.org";

    public static void main(String[] args) {

        for(String arg: args){

            if(check(arg)){

                System.out.println("in");

            }else{

                System.out.println("ok");

            }

        }

    }

    static boolean check(String name){

        try {
            InetAddress inetAddress = InetAddress.getByName(name);

            byte[] address = inetAddress.getAddress();

            String tmp = query;

            for(byte b : address){

                int uint = b<0?b+256:b;

                tmp = uint + "." + query;

            }

            InetAddress inetAddress1 = InetAddress.getByName(tmp);

            return true;

        } catch (UnknownHostException e) {
            return false;
        }


    }

}
