package com;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;

public class SourceViewer {

    public static void main(String[] args) {

        if(args.length > 0){

            try {
                URL url = new URL(args[0]);

                URLConnection urlConnection = url.openConnection();

                try(InputStream in = urlConnection.getInputStream()){

                    Reader reader = new InputStreamReader(in);

                    int c;

                    while ((c = reader.read()) != -1)
                        System.out.print((char)c);



                }

            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }

        }

    }

}
