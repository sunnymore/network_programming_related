package com;

import java.io.IOException;
import java.net.InetAddress;
import java.net.Socket;
import java.net.UnknownHostException;

public class PortScanner {

    public static void main(String[] args) {

        //String host = args.length>0?args[0] : "localhost";

        InetAddress host = null;

        try {
            host = InetAddress.getByName(args[0]);
        } catch (UnknownHostException e) {
            e.printStackTrace();
        }

        for(int i=1;i<=1024;i++){
            System.out.println(host);
            try (Socket socket = new Socket(host,i);){
                System.out.println("该机"+i+"端口TCP服务开放");
            } catch (UnknownHostException e) {
                e.printStackTrace();
            } catch (IOException e) {
                continue;
            }

        }

    }

}
