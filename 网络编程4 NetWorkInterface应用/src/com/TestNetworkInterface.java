package com;

import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.net.UnknownHostException;
import java.util.Enumeration;

public class TestNetworkInterface {

    public static void main(String[] args) {

        try {
            //通过名称获取某个网卡信息
            NetworkInterface networkInterface = NetworkInterface.getByName("en0");

            System.out.println(networkInterface);

            NetworkInterface networkInterface1 = NetworkInterface.getByName("awdl0");

            System.out.println(networkInterface1);

            NetworkInterface networkInterface2 = NetworkInterface.getByName("en4");

            System.out.println(networkInterface2);

            //通过InetAddress获取某个网卡信息
            InetAddress inetAddress = InetAddress.getLocalHost();

            NetworkInterface networkInterface3 = NetworkInterface.getByInetAddress(inetAddress);

            System.out.println(inetAddress);
            System.out.println(networkInterface3);

            InetAddress inetAddress1 = InetAddress.getLoopbackAddress();

            NetworkInterface networkInterface4 = NetworkInterface.getByInetAddress(inetAddress1);

            System.out.println(inetAddress1);

            System.out.println(networkInterface4);

            //列出所有网卡信息
            Enumeration<NetworkInterface> interfaces = NetworkInterface.getNetworkInterfaces();

            while (interfaces.hasMoreElements()){

                NetworkInterface ni = interfaces.nextElement();

                System.out.println(ni);

            }

        } catch (SocketException e) {
            e.printStackTrace();
        } catch (UnknownHostException e) {
            e.printStackTrace();
        }

    }

}
