package com;

import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.Date;
import java.util.concurrent.*;

public class PooledDayTimeServer {

    private final static int PORT = 1113;

    private final static int THREAD_NUM = 50;

    public static void main(String[] args) {

        ExecutorService tp = Executors.newFixedThreadPool(THREAD_NUM);

        try (ServerSocket serverSocket = new ServerSocket(PORT)){

            Socket conn = serverSocket.accept();

            PooledThread t = new PooledThread(conn);

            tp.submit(t);

        } catch (IOException e) {
            e.printStackTrace();
        }

    }


    public static class PooledThread implements Callable<Void>{

        private Socket conn;

        public PooledThread(Socket conn) {
            this.conn = conn;
        }

        @Override
        public Void call() {

            try {

                Writer writer = new OutputStreamWriter(conn.getOutputStream());

                writer.write(new Date().toString() + "\r\n");

                writer.flush();

            } catch (IOException e) {
                e.printStackTrace();
            } finally {

                try {
                    conn.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }

            }

            return null;

        }
    }

}
