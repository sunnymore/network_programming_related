package com;

import javax.print.DocFlavor;
import java.io.*;
import java.net.MalformedURLException;
import java.net.URL;

public class SouceViewer {

    public static void main(String[] args) {

        for(String arg : args){

            InputStream in = null;

            try {

                URL url = new URL(arg);

                in = url.openStream();

                in = new BufferedInputStream(in);

                Reader r = new InputStreamReader(in);

                int c;

                while ((c = r.read()) != -1){
                    System.out.print((char)c);
                }


            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }finally {

                if(in != null){

                    try {
                        in.close();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }

            }

        }

    }

}
